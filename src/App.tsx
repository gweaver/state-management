import React from 'react';
import styled, { createGlobalStyle } from 'styled-components'
import { ApolloProvider } from '@apollo/client';

import client from './data'
import { Header } from './components'
import { TodoList } from './containers'

const GlobalStyles = createGlobalStyle`
  body {
    background: #f5f5f5;
    color: #4d4d4d;
  }
`

const TodoApp = styled.div`
  margin: 0 auto;
  min-width: 235px;
  max-width: 550px;
`

function App() {
  return (
    <ApolloProvider client={client}>
      <TodoApp>
        <GlobalStyles />
        <Header text="Todos" />
        <TodoList />
      </TodoApp>
    </ApolloProvider>
  );
}

export default App;


