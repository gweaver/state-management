import { visibilityFilterVar } from '../cache'
import { VisibilityFilter } from '../model'

export const updateVisibilityFilter = (
  visibility: VisibilityFilter
) => visibilityFilterVar(visibility)