export enum VisibilityFilterValues {
  all = 'all',
  incompleted = 'incompleted',
  completed = 'completed'
}

export type VisibilityFilter = 'all' | 'incompleted' | 'completed'