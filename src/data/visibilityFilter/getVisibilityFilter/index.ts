import { gql } from '@apollo/client'

export const GET_VISIBILITY_FILTER = gql`
  query {
    visibilityFilter @client 
  }
`
