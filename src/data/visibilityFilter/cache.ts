import { makeVar } from '@apollo/client'

import { VisibilityFilter } from './model'

const initVisibilityFilter: VisibilityFilter = 'all' 

export const visibilityFilterVar = makeVar<VisibilityFilter>(
  initVisibilityFilter
)