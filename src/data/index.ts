import { ApolloClient } from '@apollo/client';

import { cache } from './cache'

const client = new ApolloClient({
  uri: '',
  cache: cache
});

export default client