import { gql } from '@apollo/client'

export const typeDefs = gql`
  extend type Query {
    todos: [Todo]
  }

  extend type Mutation {
    addTodo(): [Todo]
  }

  type Todo {
    id: string
    title: string
    completed: boolean
  }
`