import { InMemoryCache } from '@apollo/client';

import { todosVar } from './todos/cache';
import { visibilityFilterVar } from './visibilityFilter/cache'

export const cache: InMemoryCache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        visibilityFilter: {
          read () {
            return visibilityFilterVar();
          },
        },
        todos: {
          read (_, opts) {
            console.log(opts, 'FILTER')
            return todosVar();
          }
        }
      }
    }
  }
})
