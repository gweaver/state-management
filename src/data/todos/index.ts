import { Todos, ITodo } from './model'
import { GET_TODOS } from './getTodos'
import { createTodo } from './createTodo'
import { deleteTodo } from './deleteTodo'
import { updateTodo } from './updateTodo'

export type { Todos, ITodo }

export {
  createTodo,
  deleteTodo,
  GET_TODOS,
  updateTodo
}
