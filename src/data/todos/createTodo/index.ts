import { v4 as uuid } from 'uuid'

import { todosVar } from '../cache';

export const createTodo = (title: string) => {
  
  return  todosVar([ ...todosVar(), { id: uuid(), title, completed: false } ])
}
