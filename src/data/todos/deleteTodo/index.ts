import { todosVar } from '../cache';

export const deleteTodo = (id: string) => {
  
  const updatedTodos = todosVar().filter(
    curr => curr.id !== id 
  )
  
  return todosVar(updatedTodos)
}