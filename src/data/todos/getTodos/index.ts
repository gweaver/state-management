import { gql } from '@apollo/client'

export const GET_TODOS = gql`
  query GetTodos($visibilityFilter: String!) {
    visibilityFilter @client @export(as: "visibilityFilter")
    todos(filter: $visibilityFilter) @client {
      id
      title
      completed
    }
  }
`
