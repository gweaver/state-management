import { ReactiveVar, makeVar } from '@apollo/client'

import { Todos } from './model'

const todosInitialValue: Todos = []

export const todosVar: ReactiveVar<Todos> = makeVar<Todos>(
  todosInitialValue
);