import { todosVar } from '../cache';
import { ITodo } from '../model'

export const updateTodo = (next: ITodo) => {
    
  const updatedTodos = todosVar().map(
    curr => curr.id === next.id ? { ...curr, ...next } : curr 
  )
  
  return todosVar(updatedTodos)
}
