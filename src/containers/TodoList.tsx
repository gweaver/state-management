import React from 'react'
import styled from 'styled-components'
import { useQuery } from '@apollo/client'

import Button from '../components/Button'
import Input from '../components/Input'
import Todo from '../components/Todo'
import {
  createTodo,
  deleteTodo,
  GET_TODOS,
  ITodo,
  Todos,
  updateTodo
} from '../data/todos'
import {
  updateVisibilityFilter,
  VisibilityFilter,
  VisibilityFilterValues
} from '../data/visibilityFilter'

/** Event Handlers */

const handleUpdateVisibilityFilter = (filterBy: Partial<VisibilityFilterValues>) => (e: any) => {
  e.preventDefault()

  updateVisibilityFilter(filterBy)
}

const handleCreateTodo = (e: any) => {  
  if (e.key !== 'Enter') return;

  e.preventDefault()
  
  createTodo(e.target.value)

  e.target.value = ''
}

const handleToggleTodo = (todo: ITodo) => (e: any) => {
  e.preventDefault()

  updateTodo({
    ...todo,
    completed: !todo.completed,
  })
}

const handleDeleteTodo = (id: string) => (e: any) => {
  e.preventDefault()

  deleteTodo(id)
}

/** Renderers */

const Container = styled.div`
  display: block;
  background: #fff;
`

const buildFilters = () => Object.keys(VisibilityFilterValues).map(
  (filter: any) => <Button
    key={filter}
    text={filter}
    handleClick={handleUpdateVisibilityFilter(filter)}
  />
)


function VisibilityFilters() {
  const renderFilters = buildFilters()  
  
  return (
    <Container>
      {renderFilters}
    </Container>
  )
}

function CreateTodo() {
  return (
    <Input onKeyDown={handleCreateTodo} placeholder='What needs to get done?' />
  )
}

const renderTodos = (todos: Todos) => todos.map((todo: ITodo) => {  
  return (
    <Todo
      key={todo.id}
      deleteTodo={handleDeleteTodo(todo.id)}
      toggleCompletion={handleToggleTodo(todo)}
      {...todo}
    />
  )
})

const filterTodos = (filterBy: Partial<VisibilityFilterValues>, todos: Todos) => filterBy === VisibilityFilterValues.all ? todos : todos.filter((todo: ITodo) => filterBy === VisibilityFilterValues.incompleted && !todo.completed)

function TodosList() {
  const { data } = useQuery(GET_TODOS)

  const filteredTodos = filterTodos(data.visibilityFilter, data.todos)
  const todoList = renderTodos(filteredTodos)

  console.log(data)

  return (
    <React.Fragment>
      {todoList}
    </React.Fragment>
  )
}

function TodoList() {
  return (
    <Container>
      <CreateTodo />
      <TodosList />
      <VisibilityFilters />
    </Container>
  )
}

export default TodoList