import React from 'react'
import styled from 'styled-components'

const StyledInput = styled.input`
  position: relative;
  font-size: 24px;
  padding: 16px;
  border: none;
  background: rgba(0, 0, 0, 0.003);
  box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);
  width: 100%;
`

interface IInput {

}

function Input(props: React.InputHTMLAttributes<IInput>) {
  return (
    <StyledInput {...props} />
  )
}

export default Input