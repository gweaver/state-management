import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
  border: none;
  color: blue;
`
interface IButtonProps {
  handleClick: (event: any) => void
  text: string
}

function Button({ handleClick, text }: IButtonProps) {
  return (
    <StyledButton onClick={handleClick}>
      {text}
    </StyledButton>
  )
}

export default Button