import React from 'react'
import styled from 'styled-components'

import { ITodo } from '../data/todos'

import Button from './Button'
import Checkbox from './Checkbox'

const Container = styled.div`
  display: flex;
`

interface ITodoProps extends ITodo {
  toggleCompletion: (event: any) => void
  deleteTodo: (event: any) => void
}

function Todo({ completed, deleteTodo, title, toggleCompletion }: ITodoProps) {
  return (
    <Container>
      <Checkbox checked={completed} onChange={toggleCompletion} />
      { title }
      <Button handleClick={deleteTodo} text='x' />
    </Container>
  )
}

export default Todo