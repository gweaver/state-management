import React from 'react'
import styled from 'styled-components'

const StyledHeader = styled.h1`
  padding: 16px;
  text-align: center;
`

interface IHeader {
  text: string
}

function Header({ text }: IHeader) {
  return (
    <StyledHeader>
      {text}
    </StyledHeader>
  )
}

export default Header