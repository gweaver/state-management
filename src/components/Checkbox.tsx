import React from 'react'
import styled from 'styled-components'

const StyledInput = styled.input`

`

interface ICheckbox {

}

function Checkbox(props: React.InputHTMLAttributes<ICheckbox>) {
  return (
    <StyledInput type='checkbox' {...props} />
  )
}

export default Checkbox